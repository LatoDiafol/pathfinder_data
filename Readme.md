# Pathfinder Data

This is a project to collect as much data as possible from [d20pfsrd](https://www.d20pfsrd.com/) as possible and present it in a machine readable format.

Everything in the data directory is under the [Open Game License](https://www.d20pfsrd.com/legal/), everything else is under the MIT License

# Goals

- As much data as possible (open an issue if any is missing)
    - initially this will be limited to tabular data, but will be expanded to include descriptions etc.
- Data in a machine readable format (open an issue for new formats or missing data)
    - Planned formats: CSV, JSON, SQlite
- Automated collection of data to keep it up to date

