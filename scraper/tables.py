from typing import List, Tuple, Union, Callable

from scraper import util


class UnstructuredTable:
    """Data structure for a table raw from d20pfsrd, makes no guarantees about form"""

    def __init__(self, headers, rows):
        self.headers = headers
        self.rows = rows

    @classmethod
    def from_raw_table(cls, rows: list):
        contains_head = lambda row: len(row.find_all("th")) > 0
        headers = [row for row in rows if contains_head(row)]
        rows = [row for row in rows if contains_head(row) == False]
        return cls(headers, rows)

    def first_head_name(self) -> str:
        try:
            return self.headers[0].th.text
        except IndexError as e:
            raise ValueError(f"No headers in table {self}")

    def get_title_and_headers(self, name=None) -> Tuple[Union[str, None], list]:
        if len(self.headers) == 1:  # Table with name and column titles
            title = Table.format_title(name) if name else None
            headers = Table.table_row_to_list(self.headers[0])

        elif len(self.headers) >= 2:
            title = Table.format_title(name) if name \
                else Table.format_title(self.first_head_name())

            headers = Table.table_row_to_list(self.headers[1])

        else:
            raise ValueError(f"Table has incorrect number of headers {self.headers}\n{self.rows[0]}")

        return title, headers


class Table:
    """Formatted table, all columns will have a header and there will only be a single title"""

    def __init__(self, title: str, headers: list, rows: List[List]):
        assert headers and rows
        # assert isinstance(headers, list)
        self.title = title
        self.headers = headers
        self.rows = rows

        longest_row = max([len(row) for row in rows])
        assert longest_row == len(headers), f"longestrow ({longest_row}) != headers len ({len(headers)})"

    @staticmethod
    def format_title(title: str) -> str:
        return title \
            .replace(" ", "_") \
            .replace(".", "") \
            .replace(",", "") \
            .lower()

    @staticmethod
    def table_row_to_list(row) -> list:
        rows = row.find_all("th") + row.find_all("td")
        return [row.text for row in rows]

    @staticmethod
    def from_unstructured_tables(tables, name=None):
        return [Table.from_unstructured_table(table, name=name) for table in tables]

    @classmethod
    def from_unstructured_table(cls, table, rows=None, name=None):
        assert isinstance(table, UnstructuredTable), f"Must be UnstructuredTable not {type(table)}"

        (title, headers) = table.get_title_and_headers(name=name)

        if rows:
            rows = [Table.table_row_to_list(row) for row in rows]
        else:
            rows = [Table.table_row_to_list(row) for row in table.rows]
        return cls(title, headers, rows)

    def __repr__(self):
        return f"{self.title}: {self.headers}"

    def get_column(self, index: int):
        return [row[index] for row in self.rows]

    def add_column(self, name: str, data: Union[list, str]):
        if not isinstance(data, list):
            data = [data] * len(self.rows)

        # Check data is of correct length
        assert len(data) == len(self.rows)
        self.rows = [row + [data[index]] for index, row in enumerate(self.rows)]

        self.headers.append(name)

    def search_table(self, input: str, column=0):
        """Searches table by exact match, defaults to column 1 which is usually the thing's name"""
        return [row for row in self.rows if row[column] == input]


def combine_tables(tables: list, title=None):
    rows = util.flatten_list([
        table.rows for table in tables
    ])

    headers = set([
        tuple(table.headers) for table in tables
    ])

    if len(headers) > 1:
        headers_str = "\n\t".join([str(x) for x in headers])
        raise ValueError(f"To combine tables all headers must be the same: \n\t{headers_str}")

    header = headers.pop()
    return Table(title, header, rows)