from scraper.parser import *
from scraper import util

BASE_URL = "https://www.d20pfsrd.com"
EQUIPMENT_URL = f"{BASE_URL}/equipment"
GOODS_AND_SERVICES_URL = f"{EQUIPMENT_URL}/goods-and-services"
ADVENTURING_GEAR_URLS = f"{GOODS_AND_SERVICES_URL}/hunting-camping-survival-gear"
ALCHEMICAL_CREATIONS_URL = f"{GOODS_AND_SERVICES_URL}/herbs-oils-other-substances/"
BOOKS_URL = f"{GOODS_AND_SERVICES_URL}/books-paper-writing-supplies/"
WEAPONS_URL = f"{EQUIPMENT_URL}/weapons"


def get() -> List[Table]:
    # TODO animals and Animal Gear
    return [get_adventuring_gear(),
            get_alchemical_creations(),
            get_books_paper_and_writing_supplies(),
            get_weapons()]


def convert_page_to_table(url, title, table_filter=lambda tables: tables, header_map=lambda headers: headers) -> Table:
    content = get_page_content(url)
    all_tables = table_filter(get_page_tables(content))
    tables = util.flatten_list([Table.from_unstructured_tables(table) for table in all_tables])

    for table in tables:
        table.add_column("Category", table.title)
        table.headers = header_map(table.headers)

    return combine_tables(tables, title=title)


def get_adventuring_gear() -> Table:
    return convert_page_to_table(ADVENTURING_GEAR_URLS, title="Adventuring Gear")


def get_alchemical_creations() -> Table:
    filter = lambda tables: tables[:1]  # Ignore last few tables
    return convert_page_to_table(ALCHEMICAL_CREATIONS_URL, title="Alchemical Creations", table_filter=filter)


def get_books_paper_and_writing_supplies() -> Table:
    filter = lambda tables: tables[:4]
    return convert_page_to_table(BOOKS_URL, title="Writing Equipment", table_filter=filter)


def get_weapons() -> Table:
    table_filter = lambda tables: tables[1:-2]
    header_map = lambda header: util.list_replace(header, "Name", 0)
    return convert_page_to_table(WEAPONS_URL, title="Weapons", table_filter=table_filter, header_map=header_map)
