import copy


def flatten_list(input: list) -> list:
    return [item for sublist in input for item in sublist]


def list_replace(input: list, new, index: int) -> list:
    copy = input.copy()  # TODO benchmark and maybe add nocopy flag
    copy[index] = new
    return copy