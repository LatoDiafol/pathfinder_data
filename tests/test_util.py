from scraper import util


def test_flatten_list():
    input = [[1, 2], [3, 4]]
    result = util.flatten_list(input)
    assert [1, 2, 3, 4] == result


