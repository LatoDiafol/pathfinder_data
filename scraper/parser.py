from bs4 import BeautifulSoup
from typing import List
import requests
from scraper.tables import UnstructuredTable, Table, combine_tables


def get_page_content(url: str) -> BeautifulSoup:
    page = requests.get(url)
    page.raise_for_status()
    soup = BeautifulSoup(page.text, "html.parser")
    return soup.find("div", {"class": "article-content"})


def get_page_tables(page: BeautifulSoup) -> List:
    tables = page.find_all("table")
    return [parse_table(table) for table in tables]


def parse_table(data: BeautifulSoup) -> List[UnstructuredTable]:
    rows = data.find_all("tr")

    # Break up table into chunks separated by headers
    chunks = chunk_list_on_child_tag(rows, "th")
    tables = [UnstructuredTable.from_raw_table(chunk) for chunk in chunks]

    return tables


def chunk_list_on_child_tag(list, tag: str, ignore_single=True):
    chunks = []
    chunk = []

    for row in list:
        if row.find(tag):
            if ignore_single and len(chunk) < 2:
                pass
            else:
                chunks.append(chunk)
                chunk = []
        chunk.append(row)

    chunks.append(chunk)
    return chunks
