import scraper


def test_adventuring_gear():
    results = scraper.pages.equipment.get_adventuring_gear()
    names = [row[0] for row in results.rows]

    assert "Bedroll" in names
    assert "comfort_and_shelter" == results.search_table("Bedroll")[0][4]
    assert "Cot" in names

    assert "Fishhook" in names
    assert "Scent cloak" in names

    assert "Candle" in names
    assert "Lamp" in names
    assert "Moonrod" in names

    assert "Absinthe (glass or bottle)" in names
    assert "Baijiu" in names

    assert "Coffee, common" in names
    assert "Tea" in names

    assert "Bread" in names
    assert "Meat" in names

    assert "Animal-repellent sack" in names
    assert "Duo saw" in names
    assert "Air bladder" in names
    assert "Map" in names


def test_get_alchemical_creations():
    results = scraper.equipment.get_alchemical_creations()
    names = [row[0] for row in results.rows]

    assert "Brain mold spores [APC]" in names
    assert "Salt [APC]" in names


def test_get_books():
    result = scraper.equipment.get_books_paper_and_writing_supplies()
    names = [row[0] for row in result.rows]

    assert "Book, adventurer’s chronicle" in names
    assert "Book, blue" in names
    assert "Book Lariat" in names
    assert "Chalk (1 piece)" in names
    assert "Inkpen" in names


def test_get_weapons():
    results = scraper.equipment.get_weapons()
    names = [row[0] for row in results.rows]

    assert "Gauntlet" in names
    assert "Battle aspergillum" in names
    assert "Club" in names
    assert "Gladius" in names
    assert "Ankus" in names
    assert "Pilum" in names
    assert "Bola" in names
    assert "Thorn (20)" in names
